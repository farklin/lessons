<?php 

require_once "../vendor/autoload.php"; 

use App\Custom\Session; 
use App\Figures\Shapes; 
use App\Figures\Circle; 
use App\Figures\Square; 
use App\Figures\Rectangle; 
use App\Figures\FigureCollections; 

// инициализация 
Session::initialization(); 
Session::set('name', "Иван");
//вывод на экран 
echo Session::get('name'); 
// уничтажение данных ссессии 
Session::destroy(); 
// проверка уничтожены ли данные ссессии 
echo Session::get('name'); 

$circle = new Circle(5); 
// echo $circle->perimeter(); 
// echo "<br>"; 
// echo $circle->square(); 

$square = new Square(5); 
// echo "<br>"; 
// echo $square->perimeter(); 
// echo "<br>"; 
// echo $square->square(); 

$rectangle = new Rectangle(5, 2); 
// echo "<br>"; 
// echo $rectangle->perimeter(); 
// echo "<br>"; 
// echo $rectangle->square(); 

$collection = new FigureCollections(); 
$collection->add($circle); 
$collection->add($square); 
$collection->add($rectangle); 

echo($collection::allSquare()); 
echo "<br>"; 
echo($collection::allPerimeter()); 