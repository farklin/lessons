<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Форма для теста git</title>
    <link rel="stylesheet" href="https://teisbubble.ru/static/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"
        integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>

<body>


    <div class="container">
        <form class="card" action="hadler.php" method="post">
            <div class="card-header p-3">
                <span class="small" id="operation"></span>
                <input type="hidden" id="a" name="a">
                <input class="form-control" placeholder="0" type="number" name="number" id="number">
            </div>
            <div class="row">
                <div class="col-md-9 col-9">
                    <div class="card-body row d-flex">
                        <button type="button" value="1" class="col-md-4 col-4 btn border button">1</button>
                        <button type="button" value="2" class="col-md-4 col-4 btn border button">2</button>
                        <button type="button" value="3" class="col-md-4 col-4 btn border button">3</button>
                        <button type="button" value="4" class="col-md-4 col-4 btn border button">4</button>
                        <button type="button" value="5" class="col-md-4 col-4 btn border button">5</button>
                        <button type="button" value="6" class="col-md-4 col-4 btn border button">6</button>
                        <button type="button" value="7" class="col-md-4 col-4 btn border button">7</button>
                        <button type="button" value="8" class="col-md-4 col-4 btn border button">8</button>
                        <button type="button" value="9" class="col-md-4 col-4 btn border button">9</button>
                        <button type="button" value="0" class="col-md-4 col-4 btn border button">0</button>
                    </div>
                </div>
                <div class="col-md-3 col-3 card-body ">
                  
                        <select name="method" id="method" class="form-control">
                            <option value="0">Выберите оператор</option>
                            <option value=1>+</option>
                            <option value=2>-</option>
                            <option value=3>/</option>
                            <option value=4>*</options>
                        </select>

                        <button id="btn_equally" type="submit" class="btn btn-primary w-100 mt-3"> =</button>

                    
                    
                </div>
            </div>
        
        </form>
    </div>


    

    <script src="../js/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"
        integrity="sha512-HGOnQO9+SP1V92SrtZfjqxxtLmVzqZpjFFekvzZVWoiASSQgSr4cw9Kqd2+l8Llp4Gm0G8GIFJ4ddwZilcdb8A=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    
    <script>    

        
        // нажатие кнопок на калькуляторе 
        $('.button').click(function(){
            $('#number').val($('#number').val() +  $(this).val() ); 
        }); 

        //
        $('#method').change(function(){
            if($('#number').val() != ""){

                a = $('#number').val(); 
                method = $("#method option:selected").text(); 

                $('#operation').text(a + method); 
                $('#a').val(a); 

                $('#number').val(''); 
             
            }
            else{
                alert('значение не может быть пустым'); 
            }

        }); 

        $("#btn_equally").click(function(){
            if($("#method option:selected").val()  != "0" && $('#a').val() != "" && $("#number").val() != ""){
         
               console.log($("#method option:selected").val()); 
               
               console.log($('#a').val() ); 
               console.log($("#number").val() ); 
                
               $.ajax({
                    method: 'post', 
                    url: 'hadler.php', 
                    data: {
                        number: $("#number").val(), 
                        a: $('#a').val(),
                        method: $("#method option:selected").val(), 

                    }, 
                    success: function(data){
                           console.log(data); 
                            $('#a').val(a); 
                            $('#number').val(data); 
                            $('#operation').text(''); 
                
                    }
               }); 
                
                return false;  
            }else{
                alert('значение не может быть пустым'); 
                return false; 
            }
        }); 


        

    </script> 

</body>