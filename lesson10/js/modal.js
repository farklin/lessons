//выбираем все теги с именем  modal
$('a[name=modal]').click(function(e) {
    //Отменяем поведение ссылки
    e.preventDefault();
    //Получаем тег A
    var id = $(this).attr('href');
    var element = $(id); 
    //Получаем ширину и высоту окна
    var winH = $(window).height();
    var winW = $(window).width();
    //Устанавливаем всплывающее окно по центру
    element.css('top', winH/2 - element.height()/2);
    element.css('left', winW/2 - element.width()/2);
    //эффект перехода
    element.fadeIn(500);
});
//если нажата кнопка закрытия окна
$('.modalwindow .close').click(function (e) {
    //Отменяем поведение ссылки
    e.preventDefault();
    $('.modalwindow').fadeOut(500);
});