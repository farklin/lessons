<?php 
    require_once 'slider.php'; 
?> 

<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Форма для теста git</title>
    <link rel="stylesheet" href="https://teisbubble.ru/static/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="../plugin/slick/slick.css">
    <link rel="stylesheet" href="../plugin/slick/slick-theme.css">
    <link rel="stylesheet" href="css/style.css">
    
</head>

<body>

    <div class="container">

        <div class="single-item">
            
        <?php
         foreach($images as  $key => $image): ?> 
            <div>
                <a href="#modal<?=$key?>" name="modal" href="<?=$image?>">
                    <img height="200" src="<?=$image ?>" alt="">
                </a>
            </div>



        <?php  endforeach; ?> 
           
        </div>

    </div>

    <?php
         foreach($images as $key => $image): ?> 

    <div id="modal<?=$key?>" class="modalwindow">
    <h2>Модель интерьера <?=$key?> </h2>
    <a href="#" class="close">X</a>

    <div class="content">
        <img src="<?=$image ?>" width="100%">
    </div>

    </div>

    <?php  endforeach; ?> 








    <script src="../js/jquery-2.2.4.min.js"></script>
    
    <script src="js/modal.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" integrity="sha512-HGOnQO9+SP1V92SrtZfjqxxtLmVzqZpjFFekvzZVWoiASSQgSr4cw9Kqd2+l8Llp4Gm0G8GIFJ4ddwZilcdb8A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

   <script>
        // Слайдер
        $('.single-item').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,

        });

    </script>
    <script src="../plugin/slick/slick.js"></script>

    </body>