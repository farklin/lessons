<?php

namespace App\Custom; 

/**
 * Класс для работы с ссессией 
 */
class Session
{   
    private static $initialization = null; 
    /**
     * Конструктор класса сесси
     */
    public function __construct()
    {}
    
    /**
     * Глабальная точка доступа
     *
     * @return $initialization // new self(); 
     */
    public static function initialization()
    {   
        if(!self::$initialization){
            self::$initialization = new self(); 
            session_start(); 
        }
        return self::$initialization; 
    }

    /**
     * Проверка вводимых данных 
     *
     * @param string $name
     * @return void
     */
    public static function validate($key)
    {
        if(isset($key) and $key != ""){
            if(isset($_SESSION[$key])) return true; 
        }
        return false; 
    }


    /**
     *  Получение значения сессии 
     *
     * @param string $key // Ключ ассоциативного массива 
     * @return $value 
     */
    public static function get(string $key)
    {
        if(self::validate($key)){
            return $_SESSION[$key];
        }
    }

    /**
     * Добавление значения в сессию 
     *
     * @param string $key // Ключ ассоциативного массива 
     * @param [type] $value // Значение 
     * @return void
     */
    public static function set(string $key, $value)
    {
        if(!empty($value) and !empty($key)){
            $_SESSION[$key] = $value; 
        }
    }
    
    /**
     * Удаление ключа сессии 
     *
     * @param [string] $key // Значение 
     * @return void
     */
    public static function delete(string $key)
    {
        if(self::validate($key)){
            unset($_SESSION[$key]); 
        }
    }
    
    public static function destroy()
    {
        session_unset(); 
        session_destroy();
    }


}