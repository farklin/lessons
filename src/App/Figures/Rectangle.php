<?php

namespace App\Figures; 

use App\Figures\Shapes;

/**
 * Класс прямоугольник
 */
class Rectangle implements Shapes 
{
    public $width = 0;
    public $height = 0; 

    /**
     * Конструктор класса прямоугольник
     *
     * @param float $width
     * @param float $height
     */
    public function __construct(float $width, float $height)
    {
        $this->setWidth($width); 
        $this->setHeight($height); 
    }
    /**
     * Задает длину прямоугольника 
     *
     * @param [foat] $width
     * @return void
     */
    public function setWidth(float $width)
    {   
        if(isset($width)){
            $this->width = $width; 
        }
    }

    /**
     * Задает высоту прямоугольника 
     *
     * @param float $height
     * @return void
     */
    public function setHeight(float $height)
    {
        if(isset($height)){
            $this->height = $height; 
        }
    }
    
    /**
     * Площадь прямоугольника
     *
     * @param float $height
     * @return void
     */
    public function square()
    {
        return $this->width * $this->height; 
    }

    /**
     * Периметр прямоугольника
     *
     * @return void
     */
    public function perimeter()
    {
        return 2 * ($this->width + $this->height); 
    }  
}