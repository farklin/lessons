<?php

namespace App\Figures;

use App\Figures\Shapes; 

class FigureCollections
{   
    public static $collection = []; 
    
    public function __construct()
    {}

    /**
     * Добавить фигуру в коллекцию 
     *
     * @param [наследник интерфейса Shapes ] $element
     * @return void
     */
    public static function add($element)
    {
        if(isset($element) and $element instanceof Shapes){
            self::$collection[] = $element; 
        }
    }
    
    /**
     * Проверка возиможно ли произвести расчет 
     *
     * @param string $function_name
     * @return void
     */
    public static function sumExist($element, string $function_name)
    {
        if(isset($function_name) and $function_name != ""){
            return method_exists($element, $function_name); 
        }
    }
    
    /**
     * Высчитывает сумму возвращаемой функции наследников класса Shapes
     *
     * @param [string] $function_name
     * @return void
     */
    public static function sum(string $function_name)
    {
        $sum = 0; 
        foreach(self::$collection as $element){
            if(self::sumExist($element, $function_name)){
                $sum += $element->{$function_name}(); 
            }
        }
        return $sum; 
    }

    /**
     * Расчет суммы площади коллекции фигур
     *
     * @return $sum
     */
    public static function allSquare()
    {   
        return self::Sum("square"); 
    }

    /**
     * Расчет суммы периметра коллекции фигур
     *
     * @return $sum
     */
    public static function allPerimeter()
    {
        return self::Sum("perimeter"); 
    }
   

}