<?php
namespace App\Figures; 

interface Shapes
{   
    const PI = 3.14; 

    /**
     * Нахождение площади 
     *
     * @return void
     */
    public function square(); 

    /**
     * Нахождение периметра
     *
     * @return void
     */
    public function perimeter(); 
}