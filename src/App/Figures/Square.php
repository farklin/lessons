<?php

namespace App\Figures; 

use App\Figures\Shapes;  

/**
 * Класс квадрат 
 */
class Square implements Shapes 
{
    public $sideSquare = 0;

    /**
     * Конструктор квадрата 
     *
     * @param [type] $side
     */
    public function __construct($side)
    {
        $this->setSide($side); 
    }

    /**
     * Задает значение стороны квадрата
     *
     * @param [type] $side
     * @return void
     */
    public function setSide($side)
    {   
        if(isset($side)){
            $this->sideSquare = $side; 
        }
    }
    
    /**
     * Площадь квадрата 
     *
     * @return void
     */
    public function square()
    {
        return $this->sideSquare * $this->sideSquare; 
    }

    /**
     * Периметр квадрата 
     *
     * @return void
     */
    public function perimeter()
    {
        return 4 * $this->sideSquare; 
    }  
}