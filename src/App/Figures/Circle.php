<?php

namespace App\Figures; 

use App\Figures\Shapes; 

/**
 * Класс круг
 */
class Circle implements Shapes 
{
    public $radius = 0; 

    /**
     * Конструктор Круга
     *
     * @param float $radius
     */
    public function __construct(float $radius){
        $this->radius = $radius; 
    }

    public function setRadius(float $radius)
    {
        if(isset($radius)){
            $this->radius = $radius; 
        }
    }

    /**
     * Площадь круга
     *
     * @return void
     */
    public function square()
    {
        return Shapes::PI *( $this->radius * $this->radius ); 
    }

    /**
     * Периметр круга 
     *
     * @return void
     */
    public function perimeter()
    {
        return 2 * Shapes::PI * $this->radius;  
    }  
}