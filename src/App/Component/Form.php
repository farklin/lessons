<?php

namespace App\Component;


/**
 * Класс формы 
 */
class Form
{
    public $method = 'get';
    public $action = '/';
    public $inputs = [];
    public $name_button = "Отправить";
    public $html; 

    /**
     * Конструктор класса формы 
     *
     * @param string $method // метод post / get
     * @param string $action // action куда отправляется запрос 
     * @param array $inputs // поля формы
     * @return void
     */
    public function __constructs(string $method, string $action)
    {
        $this->method = $method;
        $this->action = $action;
    }

    /**
     * Добавление полей формы 
     *
     * @param [Input] $element
     * @return void
     */
    public function addInput($element)
    {
        array_push($this->inputs, $element);
    }

    /**
     * Генерация html полей и формы 
     *
     * @return 
     */
    public function view()
    {
        $this->html = "<form action = '" . $this->action . "' method = '" . $this->method . "'>";
        foreach ($this->inputs as $input) {
            $this->html .= $input->view(); 
        }
        $this->html .= "<button>" . $this->name_button . "</button>"; 
        $this->html .= "</form>";
        return $this->html;
    }

    /**
     * Вывод на экран формы и полей 
     *
     * @return void
     */
    public function render()
    {
        echo $this->view();
    }
}
