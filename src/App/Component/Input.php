<?php

namespace App\Component;

/**
 * Базовый класс Input
 */
class Input
{
    protected $type = "text";
    protected $value = "";
    protected $placeholder = "";
    protected $html; 

    public function __construct(string $value = "", string $placeholder = "")
    {
        $this->value = $value;
        $this->placeholder = $placeholder;
    }

    /**
     * Генерация html кода элемента
     *
     * @return void
     */
    public function view()
    {
        $this->html = "<input ";
        foreach ($this as $key => $value) {
            $this->html .= $key . " = " . "'" . $value . "'";
        }
        $this->html .= '>';
        return $this->html; 
    }

    /**
     * Вывод элемента на экран
     *
     * @return void
     */
    public function render()
    {
        echo $this->view(); 
    }



}

/**
 * Класс элемента Password 
 */
class Password extends Input
{
    public function __construct()
    {
        parent::__construct();
        $this->type = "password";
    }
}

/**
 * Класс элемента Email
 */
class Email extends Input
{
    protected $size;
    protected $maxlength;


    public function __construct()
    {
        parent::__construct();
        $this->type = "email";
    }

 
    public function setSize(int $size)
    {
        $this->size = $size;
    }

     /**
     * Установка значения ограничения символов 
     *
     * @param int $maxlength /
     */
    public function setMaxlenght(string $maxlength)
    {
        $this->maxlength = $maxlength;
    }
}


/**
 * Класс Select
 */
class Select extends Input
{
    protected $values = [];

    /**
     * Конструктор класса 
     *
     * @param [string] $value // Выбранное значение 
     * @param [array] $values // ассоциативный массив ключ значения ["key" => "value", "key" => "value"]
     */
    public function __construct(string $value, array $values)
    {
        parent::__construct($value = $value);
        $this->values = $values;

    }

    /**
     * Генерация html кода элемента 
     *
     * @return $this->html
     */
    public function view()
    {
        $select = "";
        $this->html = "<select>";
        foreach ($this->values as $key => $value) {
            $select = ""; 
            if ($key == $this->value) {
                $select = "selected";
            }
            $this->html .= "<option " . $select . " value='" . $key . "'>" . $value . "</option>";
        }
        $this->html .= '</select>';
        return $this->html; 
    }

}
