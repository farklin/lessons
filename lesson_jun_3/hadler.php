<?php
include 'include/FileUpload.php';
include 'include/Csv.php';
require_once "../vendor/autoload.php";


if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $data = [];

    //Валидация данных
    if (empty($_POST['login'])  or !isset($_POST['login'])) {
        $data['error']['login'] = "Заполните поле логин";
    }

    if (empty($_POST['email'])  or !isset($_POST['email'])) {
        $data['error']['email'] = "Заполните поле email";
    }

    if (empty($_FILES['image']['name'])  or !isset($_FILES['image'])) {
        $data['error']['image'] = "Требуется добавить картинку";
    } else {
        $load = new FileUpload('image');
        $error = $load->loadFileOnServer();
        if (empty($load->error)) {
            $filename = $load->getPullPathFileName();
        } else {
            $data['error']['image'] = $result_file;
        }
    }

    // Успешно пройденая валидация 
    if (empty($data['error'])) {
        $row = [
            $_POST['login'],
            $_POST['email'],
            $filename,
        ];

        //Добавление в таблицу 
        $csv = new Csv();
        $csv->createRow($row, 'file.csv');
        $data['success'] = 'Пользователь успешно добавлен';
    }

    echo json_encode($data);
}
