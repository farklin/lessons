<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Форма для теста git</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link href="css/style.css" rel="stylesheet" />
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <form action="" class="form mt-3" enctype="multipart/form-data">
            <h2 class="my-4">
                Создание нового пользователя
            </h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for=email">Email</label>
                        <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelpId" placeholder="">
                        <small id="erroremail" class="form-text text-danger error"></small>
                    </div>

                    <div class="form-group">
                        <label for="login">Логин</label>
                        <input type="text" class="form-control" name="login" id="login" aria-describedby="helpId" placeholder="">
                        <small id="errorlogin" class="form-text text-danger error"></small>
                    </div>
                </div>
                <div class="col-md-3 border text-center form-image">
                    <div class="form-group my-2 image-form text-center">
                        <label for="image">Выберите изображение пользователя ... </label></br>
                        <input class="inputfile" type="file" class="form-control-file" name="image" id="image" placeholder="" aria-describedby="fileHelpId">
                        <small id="errorimage" class="form-text text-danger error"></small>
                    </div>
                    <img id="blah" src="" alt="">
                </div>
            </div>
            <button type="submit" class="btn btn-primary my-4">Зарегистрировать пользователя</button>
        </form>
    </div>

    <script src="../js/jquery-2.2.4.min.js"></script>
    <script src="js/api.js"></script>
    <script src="js/preload_image.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" integrity="sha512-HGOnQO9+SP1V92SrtZfjqxxtLmVzqZpjFFekvzZVWoiASSQgSr4cw9Kqd2+l8Llp4Gm0G8GIFJ4ddwZilcdb8A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>

</html>