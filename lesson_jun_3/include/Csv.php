<?php

class Csv
{
    public function createRow(array $data, $filename)
    {
        $file = fopen($filename, 'a');
        fputcsv($file, $data);
        fclose($file);
    }
}
