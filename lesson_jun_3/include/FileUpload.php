<?php

class FileUpload
{
    protected $index;
    protected $path = __DIR__ . '/uploads/';
    protected $deny = array(
        'phtml', 'php', 'php3', 'php4', 'php5', 'php6', 'php7', 'phps', 'cgi', 'pl', 'asp',
        'aspx', 'shtml', 'shtm', 'htaccess', 'htpasswd', 'ini', 'log', 'sh', 'js', 'html',
        'htm', 'css', 'sql', 'spl', 'scgi', 'fcgi', 'psd'
    );
    protected $allow = array(
        'jpeg', 'jpg', 'png'
    );
    protected $file;
    public $error;
    protected $parths;
    public $name;
    protected $success;

    public function __construct($index, $path = '')
    {
        $this->index = $index;
        if (!empty($path)) {
            $this->path = $path;
        }
        $this->file = $_FILES[$index];
    }

    /**
     * Получение имени файла 
     *
     * @return void
     */
    public function getName()
    {
        return $this->name;
    }

    public function getPullPathFileName()
    {
        return $this->path . '/' . $this->name;
    }

    /**
     * Загрузка файлов на сервер 
     *
     * @return void
     */
    public function loadFileOnServer()
    {
        $this->existPathOrCreate();
        $this->renameFile();
        if (empty($this->validate())) {
            $this->loadFile();
            if (!empty($this->success)) {
                $result = $this->success;
            } else {
                $result = $this->error;
            }
        } else {
            $result = $this->error;
        }
        return $result;
    }

    /**
     * Переименование файла 
     *
     * @return void
     */
    protected function renameFile()
    {
        // Оставляем в имени файла только буквы, цифры и некоторые символы.
        $pattern = "[^a-zа-яё0-9,~!@#%^-_\$\?\(\)\{\}\[\]\.]";
        $this->name = mb_eregi_replace($pattern, '-', $this->file['name']);
        $this->name = mb_ereg_replace('[-]+', '-', $this->name);

        // Т.к. есть проблема с кириллицей в названиях файлов (файлы становятся недоступны).
        // Сделаем их транслит:
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',    'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',    'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',    'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',    'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',  'ь' => '',    'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',    'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',    'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',    'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',    'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',  'Ь' => '',    'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );

        $this->name = strtr($this->name, $converter);
        $this->parths = pathinfo($this->name);

        $i = 0;
        $prefix = '';
        while (is_file($this->path . $this->parths['filename'] . $prefix . '.' . $this->parths['extension'])) {
            $prefix = '(' . ++$i . ')';
        }
        $this->name = $this->parths['filename'] . $prefix . '.' . $this->parths['extension'];
    }

    /**
     * Основная функция валидации 
     *
     * @return void
     */
    public function validate()
    {
        if (!empty($this->file['error']) || empty($this->file['tmp_name'])) {
            $this->validateSystemError();
        } elseif ($this->file['tmp_name'] == 'none' || !is_uploaded_file($this->file['tmp_name'])) {
            $this->error = 'Не удалось загрузить файл.';
        } else {
            $this->validateType();
        }
        return $this->error;
    }

    /**
     * Проверка системных оршибок при загрузке файлов 
     *
     * @return void
     */
    protected function validateSystemError()
    {
        switch (@$this->file['error']) {
            case 1:
            case 2:
                $this->error = 'Превышен размер загружаемого файла.';
                break;
            case 3:
                $this->error = 'Файл был получен только частично.';
                break;
            case 4:
                $this->error = 'Файл не был загружен.';
                break;
            case 6:
                $this->error = 'Файл не загружен - отсутствует временная директория.';
                break;
            case 7:
                $this->error = 'Не удалось записать файл на диск.';
                break;
            case 8:
                $this->error = 'PHP-расширение остановило загрузку файла.';
                break;
            case 9:
                $this->error = 'Файл не был загружен - директория не существует.';
                break;
            case 10:
                $this->error = 'Превышен максимально допустимый размер файла.';
                break;
            case 11:
                $this->error = 'Данный тип файла запрещен.';
                break;
            case 12:
                $this->error = 'Ошибка при копировании файла.';
                break;
            default:
                $this->error = 'Файл не был загружен - неизвестная ошибка.';
                break;
        }
    }

    /**
     * Проверка типа файла
     *
     * @return void
     */
    protected function validateType()
    {
        if (empty($this->name) || empty($this->parths['extension'])) {
            $this->error = 'Недопустимый тип файла';
        } elseif (!empty($this->allow) && !in_array(strtolower($this->parths['extension']), $this->allow)) {
            $this->error = 'Недопустимый тип файла';
        } elseif (!empty($this->deny) && in_array(strtolower($this->parths['extension']), $this->deny)) {
            $this->error = 'Недопустимый тип файла';
        } else {
            $this->error = '';
        }
    }

    /**
     * Проверка статуса валидации
     *
     * @return bool
     */
    public function validateStatus()
    {
        $this->error = $this->validate();
        return empty($this->error);
    }
    /**
     * Проверкс существования папки загрузки 
     *
     * @return void
     */
    public function existPathOrCreate()
    {
        if (!is_dir($this->path)) {
            mkdir($this->path, 0777, true);
        }
    }

    protected function loadFile()
    {
        // Перемещаем файл в директорию.
        if (move_uploaded_file($this->file['tmp_name'], $this->path . $this->name)) {
            // Далее можно сохранить название файла в БД и т.п.
            $this->success = 'Файл «' . $this->name . '» успешно загружен.';
        } else {
            $this->error = 'Не удалось загрузить файл.';
        }
    }
}
