$(".form").submit(function (e) {
  e.preventDefault();

  var form = new FormData(this);

  var settings = {
    url: "http://localhost/lessons/lesson_jun_3/hadler.php",
    method: "POST",
    timeout: 0,
    processData: false,
    mimeType: "multipart/form-data",
    contentType: false,
    data: form,
  };
  $.ajax(settings).done(function (response) {
    data = $.parseJSON(response);
    errors = data.error;
    success = data.success;

    $(".error").removeClass("active");
    for (var key in errors) {
      $("#error" + key).html(errors[key]);
      $("#error" + key).addClass("active");
    }

    if (!errors) {
      alert(success);
    }
  });
});
