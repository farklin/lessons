﻿<?php 

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$host = "127.0.0.1";
$user = "root"; 
$pasword = "";
$database = "test_hb";

// $link = mysqli_connect($host, $user, $pasword) or die(mysqli_error($link)); 
// $query = "CREATE DATABASE test_hb";
// $result = mysqli_query($link, $query) or die(mysqli_error($link)); 
// if($result)
// {
//     echo "База данных успешно создана"; 
// }
// mysqli_close($link);


// подключение к базе данных
$link = new mysqli($host, $user, $pasword, $database) or die(mysqli_error($link)); 
$query = "
    SHOW TABLES 
";
$result = mysqli_query($link, $query) or die(mysqli_error($link)); 
if($result)
{
    echo "<br>" ."<strong> Вывод всех таблиц из базы данных </strong>". "<br>"; 

    while ( $tables = $result->fetch_assoc())
    {
       foreach($tables as $table){
           echo $table . "<br>"; 
       }
    }
}


echo "<br>" ."<strong> Вывод значений таблицы Page </strong>". "<br>"; 
$query = "
    SELECT * FROM Page; 
";
$result = mysqli_query($link, $query) or die(mysqli_error($link)); 
if($result)
{
    while ( $row = $result->fetch_assoc())
    {
        foreach($row as $key => $value){
            echo "<strong>" . $key . "</strong>:" . $value . "<br>";
        }
    }
}


mysqli_close($link);
