<?php 

require_once "../vendor/autoload.php"; 

use App\Component\Form; 
use App\Component\Input;
use App\Component\Select;
use App\Component\Password;
use App\Component\Email;


// обьявление полей формы 
$text = new Input();
$email = new Email();
$password = new Password();
$select = new Select("123", ["12" => "Одежда", "123" => "Куртки", "12512" => "Кофты" ]);

// Создание формы 
$form = new Form(); 
$form->addInput($text); 
$form->addInput($email); 
$form->addInput($password); 
$form->addInput($select); 

//Вывод формы на экран 
$form->render(); 


