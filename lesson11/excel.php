<?php

require_once "../vendor/autoload.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Тестовый класс 
 * 
 * Пример использования класса 
 *  $excel = new Excel();
 *  $excel->createBook();
 *  $excel->sheetActiv(); 
 *  $excel->writeCell('A1', 'Класс');
 *  $excel->writeCell('B2', 'Hello world');
 *  $excel->save("new_file.xlsx");
 * 
 */
class Excel
{
    public function __construct()
    {
    }

    /**
     * Создание книги 
     *
     * @return void
     */
    public function createBook()
    {
        $this->spreadsheet = new Spreadsheet();
    }

    /**
     * Перейти на активный лист 
     *
     * @return void
     */
    public function sheetActiv()
    {
        if (!empty($this->spreadsheet)) {
            $this->sheet = $this->spreadsheet->getActiveSheet();
        }
    }

    /**
     * Запись в ячейку 
     *
     * @param string $cell // координаты в формате "A6", "B2"
     * @param string $text  // текст который необходимо записать 
     * @return void
     */
    public function writeCell(string $cell, string $text)
    {
        if (!empty($this->sheet)) {
            $this->sheet->setCellValue($cell, $text);
        }
    }

    /**
     * Сохранение файла Excel
     *
     * @param string $namefile // Имя файла для сохранения 
     * @return void
     */
    public function save(string $namefile)
    {
        if (!empty($this->spreadsheet)) {
            $writer = new Xlsx($this->spreadsheet);
            $writer->save($namefile);
        }
    }
}
