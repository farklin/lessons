<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="Главная страница" />
    <meta property="og:description" content="Альтернативная страница полное описание" />
    <meta property="og:image" content="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Marilyn_Monroe_-_publicity.JPG/210px-Marilyn_Monroe_-_publicity.JPG" />
    <meta property="og:type" content="profile" />
    <meta property="og:url" content="http://localhost/lessons/lesson11/" />

    <title>Главная страница</title>
</head>

<body>
    <div itemscope itemtype="http://schema.org/NewsArticle">
        <h1 itemprop="headline">Выход нового сериала "Игра в кальмара"</h1>
        <p itemprop="author" itemscope itemtype="http://schema.org/Person">
            Автор: <span itemprop="name">Хван Дон-хек</span> —
            <span itemprop="jobTitle">режиссер </span> </p>
        <p itemprop="articleBody">
            Режиссёром и автором сценария стал Хван Дон Хёк. Сериал, состоящий из девяти эпизодов, повествует о группе людей, которые из-за нужды в деньгах принимают приглашение об участии в тайной игре на выживание. 17 сентября 2021 года состоялась мировая премьера сериала на платформе Netflix.
        </p>
    </div>
</body>

</html>